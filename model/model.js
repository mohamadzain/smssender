Messenger = new Mongo.Collection('messenger');
Messages = new Mongo.Collection('messages');
Sms = new Mongo.Collection('sms');
Iframes = new Mongo.Collection('iframes');
Meetings = new Mongo.Collection('meetings');
Contacts = new Mongo.Collection('contacts');


FS.HTTP.setBaseUrl('/resource');

Web = new FS.Collection('web', {
    stores: [
        new FS.Store.GridFS('web', {
            transformWrite: myTransformWriteFunction //optional
        })
    ],
    filter: {
        allow: {
            contentTypes: ['text/html', 'image/*', 'audio/*', 'video/*']
        }
    }
});

function myTransformWriteFunction(fileObj, readStream, writeStream) {
    if((/html/g).test(fileObj.original.type)) {
        readStream.on('data', (chunk) => {
            // var bundlePath = 'http://' + Meteor.settings.public.hostIP + '/bundle.js';
            // var edited = chunk.toString('utf8').replace(/bundle\.js/, bundlePath);
            var jsPath = 'src="http://' + Meteor.settings.public.hostIP + '/';
            var transform1 = chunk.toString('utf8').replace(/src=\"/g, jsPath);
            var cssPath = 'href="http://' + Meteor.settings.public.hostIP + '/app.css"';
            var transform2 = transform1.toString('utf8').replace(/href=\"app.css\"/, cssPath);

            writeStream.end(transform2);
        });
    }else {
      readStream.pipe(writeStream);
    }
}
