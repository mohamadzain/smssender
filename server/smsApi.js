Meteor.publish('newSMSCollections', function() {
    // if(this.userId) {
    return Sms.find({
        status: 'New'
    });
    // } else {
    // this.ready();
    // }
});

Meteor.publish('smsCollections', function() {
    if(this.userId) {
        return Sms.find({});
    } else {
        this.ready();
    }
});



// Enable cross origin requests for all endpoints

/*
 * body of message from POST is a json object
 * {
 *   recipient : String
 *   content : String
 * }
 */
/*jshint -W106*/
Meteor.method('smsSend', (sms) => {
    var result = {};
    if(sms) {
        result = _.extend(result, sms);
        result._id = Sms.insert(sms);
    }
    return [result];
}, {
    url: 'api/sms',
    httpMethod: 'post',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        /*authenticate if req from username exists*/
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        /*if user exists add other properties*/
        var sms = {};
        if(messenger) {
            sms.requestTime = moment().utc().format();
            sms.sentTime = null;
            sms.status = 'New'; // New| Sent | Completed | Expired
            sms.content = req.body.content;
            sms.message = req.body.message;
            sms.recipient = req.body.recipient;
            sms.sender = messenger.username;
            return [sms];
        } else {
            return null;
        }
    }
});


/*
 * return sms by id
 */



/*
 * return sms by id
 */
Meteor.method('smsStatus', (sms) => {

    if(sms) {
        return sms;
    } else {
        return [];
    }
}, {
    url: 'api/sms/status?',
    httpMethod: 'get',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
          console.log(messenger);
        if(messenger) {
            var sort = -1;
            if(req.query.sort) {
                sort = req.query.sort;
            }
            var limit = 20;
            if(req.query.limit) {
                limit = req.query.limit;
            }
            var result = [];
            if(req.query._id) {
                result = Sms
                    .findOne({
                        _id: req.query._id
                    });
            } else
            if(req.query.recipient) {
                result = Sms
                    .find({
                        recipient: req.query.recipient
                    }, {
                        sort: {
                            requestTime: sort
                        },
                        limit: limit
                    })
                    .fetch();
            } else
            if(req.query.sender) {
                result = Sms
                    .find({
                        sender: req.query.sender
                    }, {
                        sort: {
                            requestTime: sort
                        },
                        limit: limit
                    })
                    .fetch();
            }
            if(req.query.state) {
                result = Sms
                    .find({
                        status: req.query.state
                    }, {
                        sort: {
                            requestTime: sort
                        },
                        limit: limit
                    })
                    .fetch();
            }
            if(req.query.signage) {
                Sms
                    .find({}, {
                        sort: {
                            requestTime: sort
                        },
                        limit: limit
                    })
                    .forEach(function(sms) {
                        var start = new Date(moment(sms.requestTime));
                        var end = new Date();
                        var diff = moment.range(start, end).diff('minutes');
                        if(diff < 5 ) {
                            result.push(sms);
                        }
                    });
            }
            return [result];
        } else {
            return null;
        }
    }
});


Meteor.method('deleteAllsms', () => {
    return [];
}, {
    url: 'api/smses',
    httpMethod: 'delete',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        if(messenger === null) {
            return null;
        } else {
            Sms
                .remove({
                    sender: req.body.owner
                });
            return null;
        }

    }
});



/*
 * return sms by id
 */
Meteor.method('modemStatus', (status) => {
    return status;
}, {
    url: 'api/modem/status?',
    httpMethod: 'get',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });

        if(messenger === null) {
            return null;
        } else {
            var result = Messenger
                .find({}, {
                    sort: {
                        updated: -1
                    },
                    limit: 1
                })
                .fetch();
            return [result];
        }
    }
});



// Sms
//   .find()
//   .observe({
//     addedAt: (doc) => {
//       console.log(doc);
//     }
//   });
