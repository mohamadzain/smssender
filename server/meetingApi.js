Meteor.publish('meetingsCollections', function() {
    // if(this.userId) {
    return Meetings.find({});
    // } else {
    //     this.ready();
    // }
});


/*jshint -W106*/
Meteor.method('meetingsSignage', (meeting) => {
    var result = {};
    if(meeting) {
        result = _.extend(result, meeting);
        result._id = Meetings.insert(meeting);

    }
    return result;
}, {
    url: 'api/meeting',
    httpMethod: 'post',
    getArgsFromRequest: (req) => {
              check(req.headers['x-token'], String);
        /*authenticate if req from username exists*/
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        /*if user exists add other properties*/
        var meeting = {};
        if(messenger) {
            if(req.body.room) {
                Meetings.remove({
                    room: req.body.room
                });
            }
            meeting.room = req.body.room;
            meeting.title = req.body.title;
            meeting.details = req.body.details;

            return [meeting];
        } else {
            return null;
        }
    }
});


Meteor.method('deleteAllMeetings', () => {
    return [];
}, {
    url: 'api/meetings',
    httpMethod: 'delete',
    getArgsFromRequest: (req) => {
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        if(messenger === null) {
            return null;
        }
        Meetings
            .remove({});
        return null;
    }
});
