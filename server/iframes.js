// server
Meteor.publish('iframeCollections', function() {
    // if(this.userId) {
        return Iframes.find({});
    // } else {
        // this.ready();
    // }
});


Meteor.methods({
    newSignageIframe: function(iframe) {
        Iframes.insert(iframe, (err) => {
            if(err) {
                return err;
            }
        });
    },
    removeSignageIframe: function(iframe) {
        Iframes.remove({
            _id: iframe._id
        }, (err) => {
            if(err) {
                return err;
            }
        });
    },
    updateSignageIframe: function(iframe) {
        Iframes.update({
            _id: iframe[0]._id
        }, iframe[0], {}, (err) => {
            if(err) {
                return err;
            }
        });
    }
});
