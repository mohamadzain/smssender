RssFeed.publish('sms', function() {
    var self = this;
    // We got 3 helpers:
    // 1. self.setValue
    // 2. self.addItem
    // 3. self.cdata


    // query is the parsed querystring as an object
    // eg. foo=latest would be query.foo === 'latest'

    // feed handler helpers
    // this.cdata, this.setValue, this.addItem
  self.setValue('title','New Message');
  self.setValue('description', 'This is a live message feed');
    //  self.setValue('link', 'http://mysite.meteor.com');
  //  self.setValue('lastBuildDate', new Date());
  //  self.setValue('pubDate', new Date());
    self.setValue('ttl', 1800);
    // managingEditor, webMaster, language, docs, generator

    Sms.find({})
        .forEach(function(sms) {
            var start = new Date(moment(sms.requestTime));
            var end = new Date();
            var diff = moment.range(start, end).diff('minutes');
            if(diff < 5) {
                self.addItem({
                    time: new Date(moment(sms.requestTime)),
                    sender: sms.sender,
                    recipient: sms.recipient,
                    content: sms.content
                });
            }
        });
});
