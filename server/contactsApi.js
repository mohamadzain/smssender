Meteor.publish('contactsCollections', function() {
    // if(this.userId) {
    return Contacts.find({});
    // } else {
    //     this.ready();
    // }
});


/*jshint -W106*/
Meteor.method('newContact', (contact) => {
    var result = {};
    if(contact) {
        result = _.extend(result, contact);
        result._id = Contacts.insert(contact);
    }
    return [result];
}, {
    url: 'api/contact',
    httpMethod: 'post',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        /*authenticate if req from username exists*/
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        /*if user exists add other properties*/
        var contact = {};
        if(messenger) {
            contact.mobile = req.body.mobile;
            contact.owner = req.body.owner;
            contact.timestamp = moment().utc().format();
            return [contact];
        } else {
            return null;
        }
    }
});


/*jshint -W106*/
Meteor.method('updateContact', (contact) => {
    var result = {};
    if(contact) {
        result = _.extend(result, contact);
        if(contact._id) {
            Contacts.update({
                _id: contact._id
            }, {
                $set: {
                    mobile: contact.mobile,
                    timestamp: moment().utc().format()
                }
            });
        }
    }
    return [result];
}, {
    url: 'api/contact',
    httpMethod: 'put',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        /*authenticate if req from username exists*/
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        /*if user exists add other properties*/
        var contact = {};
        if(messenger) {
            if(req.body._id) {
                contact = Contacts.findOne({
                    _id: req.body._id
                });
                if(contact) {
                    contact.mobile = req.body.mobile;
                }
            }
            return [contact];
        } else {
            return null;
        }
    }
});



Meteor.method('listContacts', (contacts) => {
    if(contacts) {
        return contacts;
    } else {
        return [];
    }
}, {
    url: 'api/contacts?',
    httpMethod: 'get',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        if(messenger === null) {
            return null;
        }
        var sort = -1;
        if(req.query.sort) {
            sort = req.query.sort;
        }
        var limit = 20;
        if(req.query.limit) {
            limit = req.query.limit;
        }
        var result = [];
        result = Contacts
            .find({
                owner: req.query.owner
            }, {
                sort: {
                    timestamp: sort
                },
                limit: limit
            })
            .fetch();
        return [result];
    }
});

Meteor.method('deleteContact', () => {
    return [];
}, {
    url: 'api/contact',
    httpMethod: 'delete',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        if(messenger === null) {
            return null;
        }
        Contacts
            .remove({
                _id: req.body._id
            });
        return null;
    }
});


Meteor.method('deleteAllContacts', () => {
    return [];
}, {
    url: 'api/contacts',
    httpMethod: 'delete',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        if(messenger === null) {
            return null;
        }
        Contacts
            .remove({
                owner: req.body.owner
            });
        return null;
    }
});
