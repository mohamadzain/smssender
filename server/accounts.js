var superUser = Accounts.findUserByEmail('zain@zymplsi.com');

if(!superUser) {
    var userId = Accounts.createUser({
        email: 'zain@zymplsi.com',
    });
    Accounts.setPassword(userId, '12345');
    Meteor.users.update({
        _id: userId
    }, {
        $set: {
            isSuper: true,
            isAdmin: true,
            isUser: true
        }
    });
}

// server
Meteor.publish('expandedUserCollections', function() {
    if(this.userId) {
        return Meteor.users.find({}, {
            fields: {
                isSuper: 1,
                isAdmin: 1,
                isUser: 1,
                emails: 1,
                type: 1
            }
        });
    } else {
        this.ready();
    }
});

Meteor.publish('expandedDeviceCollections', function() {
    if(this.userId) {
        return Meteor.users.find({}, {
            fields: {
                username: 1,
                type: 1,
                token: 1
            }
        });
    } else {
        this.ready();
    }
});



Meteor.methods({
    newUserAccount: function(account) {
        var newUserId = Accounts.createUser({
            email: account.email,
            password: account.password,
        });
        Meteor.users.update({
            _id: newUserId
        }, {
            $set: {
                type: account.type,
                isAdmin: account.isAdmin,
                isUser: account.isUser
            }
        });
        if(newUserId) {
            return newUserId;
        }
    },
    removeUserAccount: function(id) {
        Meteor.users.remove({
            _id: id
        });
        return id;
    },
    newDeviceAccount: function(account) {
        var newUserId = Accounts.createUser({
            username: account.username,
        });
        var token = Random.id();
        Meteor.users.update({
            _id: newUserId
        }, {
            $set: {
                type: account.type,
                token: token
            }
        });
        if(newUserId) {
            return token;
        }
    },
    removeDeviceAccount: function(id) {
        Meteor.users.remove({
            _id: id
        });
        return id;
    },
});


Meteor.method('accountInfo', (info) => {
    if(info) {
        return [info];
    } else {
        return [];
    }
}, {
    url: 'api/account/info',
    httpMethod: 'get',
    getArgsFromRequest: (req) => {

        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        return [messenger];
    }
});
