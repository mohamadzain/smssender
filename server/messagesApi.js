Meteor.publish('messageCollections', function() {
    // if(this.userId) {
    return Messages.find({});
    // } else {
    // this.ready();
    // }
});


/*jshint -W106*/
Meteor.method('newMessage', (message) => {
    var result = {};
    if(message) {
        result = _.extend(result, message);
        result._id = Messages.insert(message);
    }
    return [result];
}, {
    url: 'api/message',
    httpMethod: 'post',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        /*authenticate if req from username exists*/
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        /*if user exists add other properties*/
        var message = {};
        if(messenger) {
            message.content = req.body.content;
            return [message];
        } else {
            return null;
        }
    }
});
//

/*jshint -W106*/
Meteor.method('updateMessage', (message) => {
    var result = {};
    if(message) {
        result = _.extend(result, message);
        if(message._id) {
            Messages.update({
                _id: message._id
            }, {
                $set: {
                    content: message.content,
                }
            });
        }
    }
    return [result];
}, {
    url: 'api/message',
    httpMethod: 'put',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        /*authenticate if req from username exists*/
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        /*if user exists add other properties*/
        var message = {};
        if(messenger) {
            if(req.body._id) {
                message = Messages.findOne({
                    _id: req.body._id
                });
                if(message) {
                    message.content = req.body.content;
                }
            }
            return [message];
        } else {
            return null;
        }
    }
});



Meteor.method('listMessages', (messages) => {
    if(messages) {
        return messages;
    } else {
        return [];
    }
}, {
    url: 'api/messages',
    httpMethod: 'get',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        if(messenger === null) {
            return null;
        }
        var result = [];
        result = Messages
            .find({})
            .fetch();
        return [result];
    }
});

Meteor.method('deleteMessage', () => {
    return [];
}, {
    url: 'api/message',
    httpMethod: 'delete',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        if(messenger === null) {
            return null;
        }
        Messages
            .remove({
                _id: req.body._id
            });
        return null;
    }
});


Meteor.method('deleteAllMessages', () => {
    return [];
}, {
    url: 'api/messages',
    httpMethod: 'delete',
    getArgsFromRequest: (req) => {
        check(req.headers['x-token'], String);
        var messenger = Meteor.users
            .findOne({
                token: req.headers['x-token']
            });
        if(messenger === null) {
            return null;
        }
        Messages
            .remove({});
        return null;
    }
});
