const spawn = Meteor.npmRequire('child_process').spawn;


var hostIPQuery = spawn('printenv', ['DOCKER_HOST']);
hostIPQuery.stdout.on('data', Meteor.bindEnvironment((data) => {
    var ip = data.toString('utf8').replace('\x0A', '');
    Meteor.settings.public = {
        hostIP: ip
    };
}));
