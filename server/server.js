 Meteor.startup(() => {
     /*jshint -W003*/
     'use strict';

     var device = '/dev/ttyS0';
     // var device = '/dev/tty';

     var baudrate = 115200;
     var serialPort = new SerialPort.SerialPort(device, {
         baudrate: baudrate,
     }, false);

     /*init doc to store the info and status of the port comm*/
     var thisMessenger = this;
     thisMessenger.isBusy = false;
     thisMessenger.writeWatchId = null;
     thisMessenger.Id = Messenger.insert({
         device: device,
         baudrate: baudrate,
         portOpened: false,
         modemConnected: true,
     });

     /*wrap async serial write*/
     var wrappedSerialPortWrite = Meteor.wrapAsync(serialPortWrite);

     /*wrap async serialport.open and get port status */
     var wrappedSerialPortOpen = Meteor.wrapAsync(serialPortOpen);
     var isPortOpen = wrappedSerialPortOpen();

     if(isPortOpen) {
         /*update port status to open*/
         setPortOpenedStatus(true, moment.utc().format());
         Meteor.log.info('Communication to Modem Initialised', {}, '');
         console.log('port is open');

         /*loop that cycles thru the message write to port*/
         thisMessenger.loopTimer = 1000; //1s
         thisMessenger.loopId =
             Meteor.setInterval(messengerLoop, thisMessenger.loopTimer);

         /*event listener for port incoming data*/
         serialPort.on('data', Meteor.bindEnvironment(function(data) {
             // +CMGS: 172
             // OK
             // +CDS: 6,172,'96385906',129,'16/01/04,12:44:49+32','16/01/04,12:44:50+32',0
             /*clear timeout that will fire the true flag for modem connect failure*/
             if((/OK/g).test(data)) {
                 Meteor.log.info('SMS', {
                     connect: 'OK'
                 }, data);
                 console.log('modem receive OK');
                 Meteor.clearTimeout(thisMessenger.writeWatchId);
                 thisMessenger.writeWatchId = null;
                 /*set modem connect flag as true*/
                 setModemConnectStatus(true, moment.utc().format());
             }

             /*message sent succesfully*/
             if((/\+CDS/g).test(data)) {
                 Meteor.log.info('SMS', {
                     sms: 'SUCCESS'
                 }, data);
                 thisMessenger.isBusy = false;

                 var messageSent = Sms
                     .findOne({
                         status: 'Sent'
                     });
                 updateMessageStatus(messageSent._id, 'Success', messageSent.sentTime);
             }

             if((/ERROR/g).test(data)) {
                 Meteor.log.error('SMS', {
                     sms: 'ERROR'
                 }, data);
                 thisMessenger.isBusy = false;
                 var messageSentError = Sms
                     .findOne({
                         status: 'Sent'
                     });
                 updateMessageStatus(messageSentError._id, 'Fail', messageSentError.sentTime);
             }

         }));

         /*event listener for port close unexpectedly*/
         serialPort.on('close', Meteor.bindEnvironment(() => {
             Meteor.log.fatal('Communication to Modem Failed', {}, '');
             console.log('modem port close');
             setPortOpenedStatus(false, moment.utc().format());
             setModemConnectStatus(false, moment.utc().format());
             Meteor.clearInterval(thisMessenger.loopId);
         }));

     } else {
         setModemConnectStatus(false, moment.utc().format());
         Meteor.log.fatal('Communication to Modem Fail to Initialise', {}, '');
         console.log('modem port fail to open');
     }

     /****************************************************************
      * functions
      *****************************************************************/

     function messengerLoop() {
         /*mark all fail sms*/
         Sms
             .find({
                 status: 'Sent'
             })
             .forEach((messageSent) => {
                 var start = moment(messageSent.sentTime);
                 var end = moment();
                 var range = moment.range(start, end);
                 if(range.diff('minutes') > 2) {
                     updateMessageStatus(messageSent._id, 'Fail', messageSent.sentTime);
                     Meteor.log.error('Server', {
                         sms: 'FAIL'
                     }, messageSent.recipient);
                 }
             });
         /*find any valid sent message that still waiting for ack from SC*/
         var messageUnAckSent = Sms
             .findOne({
                 status: 'Sent'
             });

         /*clear busy flag as no more sent message waiting for ack from SC*/
         if(!messageUnAckSent) {
             thisMessenger.isBusy = false;
         }

         /*get a message*/
         var messageNew = Sms
             .findOne({
                 status: 'New'
             });
         /*return if no new message*/
         if(!messageNew) {
             return;
         }
         /*mark new as expired messages if not sent after 2 minutes*/
         var start = moment(messageNew.requestTime);
         var end = moment();
         var range = moment.range(start, end);
         if(range.diff('minutes') > 2) {
             updateMessageStatus(messageNew._id, 'Expired', null);
             Meteor.log.error('Server', {
                 sms: 'EXPIRED'
             }, messageNew.recipient);
             return;
         }

         /*bypass until reply shows data sent is succesfull*/
         if(!thisMessenger.isBusy) {
             thisMessenger.isBusy = true;
             /* send sms commands and message*/
             wrappedSerialPortWrite('AT+CMGF=1\r');
             wrappedSerialPortWrite('AT+CNMI=2,2,0,1,0\r');
             wrappedSerialPortWrite('AT+CSMP=49,169,0,0\r');
             wrappedSerialPortWrite('AT+CMGS=' + messageNew.recipient + '\r');
             wrappedSerialPortWrite(messageNew.message + '\x1A');

             /* maker message as sent*/
             updateMessageStatus(messageNew._id, 'Sent', moment.utc().format());
             Meteor.log.info('Server', {
                 sms: 'SENT'
             }, messageNew.recipient);
         } /*end of thisMessenger.isBusy*/

     }

     function updateMessageStatus(id, status, time) {
         Sms.update({
             _id: id
         }, {
             $set: {
                 status: status,
                 sentTime: time
             }
         });
     }

     /*open port and return true if succesfull*/
     function serialPortOpen(callback) {
         serialPort.open((err) => {
             if(err) {
                 throw err;
             } else {
                 callback(null, true);
             }
         });
     }

     function serialPortWrite(message, callback) {
         serialPort.write(message, (err, results) => {
             if(err) {
                 throw err;
             } else {
                 callback(null, results);
             }
         });
         /*reset write watchdog time out*/
         if(thisMessenger.writeWatchId) {
             Meteor.clearTimeout(thisMessenger.writeWatchId);
             thisMessenger.writeWatchId = null;
         }

         /*timeout if not cancel by ondata event will flag modem connect fail */
         thisMessenger.writeWatchId = Meteor.setTimeout(() => {
             setModemConnectStatus(false, moment.utc().format());
             Meteor.log.warn('Modem', {
                 connect: 'TIMEOUT'
             }, '');
         }, 5000);
     }

     function setModemConnectStatus(status, time) {
         Messenger.update(thisMessenger.Id, {
             $set: {
                 modemConnected: status,
                 updated: time
             }
         });
     }

     function setPortOpenedStatus(status, time) {
         Messenger.update(thisMessenger.Id, {
             $set: {
                 portOpened: status,
                 updated: time
             }
         });
     }


 });
