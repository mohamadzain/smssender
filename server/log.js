Meteor.log.rule('File',
{
  enable: true,
  filter: ['ERROR', 'FATAL', 'WARN', 'INFO'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', '*' */
  client: false, /* This allows to call, but not execute on Client */
  server: true   /* Calls from client will be executed on Server */
});

Meteor.log.file.format = (time, level, message, data, userId) => {
  var formattedTime = moment(+time).format('HH:mm:ss');
  return (formattedTime) + ' [' + level + ']: ' + message + ' | ' + data + ' | ' + userId + ' \r\n';
};

// New file will be created every hour
Meteor.log.file.fileNameFormat = function(time) {
  var formattedTime = moment(time).format('YYYY-MM-DD_HH');
  return  (formattedTime + '.log');
};
