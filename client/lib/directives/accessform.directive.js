(() => {
    'use strict';
    /*jshint -W098*/
    /*jshint -W027*/
    angular
        .module('app.access')
        .controller('AccessFormController', AccessFormController)
        .directive('accessform', accessform);

    accessform.$inject = ['$templateRequest', '$compile'];

    function accessform($templateRequest, $compile) {
        // function accessform() {

        var directive = {
            link: AccessFormLink,
        };
        return directive;

        function AccessFormLink(scope, element) {

            changeTemplate('client/lib/views/partials/access-user.html');

            scope.$on('changeTemplate', function(ev, arg) {
                changeTemplate(arg);
            });

            function changeTemplate(template) {
                element.empty();
                $templateRequest(template)
                    .then((html) => {
                        var template = angular.element(html);
                        var content = $compile(template)(scope);
                        element.append(content);
                    });
            }

        }
    }

    AccessFormController.$inject = ['$rootScope', '$scope', '$element', '$state', '$reactive', '$mdSidenav', '$mdToast'];

    function AccessFormController($rootScope, $scope, $element, $state, $reactive, $mdSidenav, $mdToast) {
        var access = this;

        activate();

        ////////////////

        function activate() {

            let reactiveContext = $reactive(access).attach($scope);

            access.subscribe('expandedUserCollections');
            access.helpers({
                currentUser: () => {
                    var userId = Meteor.userId();
                    return Meteor.users.find({
                        _id: userId
                    });
                }
            });

            reactiveContext.autorun(() => {
                access.getReactively('currentUser');
                Session.set('isSuper', false);
                Session.set('isAdmin', false);
                Session.set('isUser', false);
                Session.set('email', '');

                if(access.currentUser.length > 0) {
                    Session.set('email', access.currentUser[0].emails[0].address);
                    Session.set('isSuper', access.currentUser[0].isSuper);
                    Session.set('isAdmin', access.currentUser[0].isAdmin);
                    Session.set('isUser', access.currentUser[0].isUser);

                }
            });

            access.templateUrl = 'client/lib/views/partials/access-user.html';


            access.submit = () => {
                Meteor.loginWithPassword(access.email, access.password, (err) => {
                    if(err) {
                        showToast(err.reason, '#userToast');
                    } else {
                        $mdSidenav('right').close();
                        $state.go('app.smssender');
                    }
                });
            };

            access.logout = () => {
                Meteor.logout();
                $scope.accessUserForm.$setPristine();
                $scope.accessUserForm.$setUntouched();
                access.email = '';
                access.password = '';
                $mdSidenav('right').close();
                $state.go('app.home');
            };

            access.saveNewPassword = () => {
                Accounts.changePassword(
                    access.passwordOld,
                    access.passwordNew, (err) => {
                        if(err) {
                            showToast(err.reason, '#userToast');
                        } else {
                            access.cancelChangePassword();
                            showToast('Password Changed!', '#userToast');

                        }
                    });
            };

            access.changePassword = () => {
                access.passwordOld = '';
                access.passwordNew = '';
                $rootScope.$broadcast('changeTemplate', 'client/lib/views/partials/access-password-change.html');
            };

            access.cancelChangePassword = () => {
                $rootScope.$broadcast('changeTemplate', 'client/lib/views/partials/access-user.html');
            };

        }


        function showToast(message, element) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .hideDelay(3000)
                .position(access.getToastPosition())
                .parent(angular.element(element))
            );
        }

        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };
        access.toastPosition = angular.extend({}, last);

        access.getToastPosition = () => {
            sanitizePosition();
            return Object.keys(access.toastPosition)
                .filter(function(pos) {
                    return access.toastPosition[pos];
                })
                .join(' ');
        };
        /*jshint -W116 */
        function sanitizePosition() {
            var current = access.toastPosition;
            if(current.bottom && last.top) current.top = false;
            if(current.top && last.bottom) current.bottom = false;
            if(current.right && last.left) current.left = false;
            if(current.left && last.right) current.right = false;
            last = angular.extend({}, current);
        }



    }


})();
