(() => {
    'use strict';

    angular
        .module('app.sidenav')
        .controller('SideNavController', SideNavController);

    SideNavController.$inject = ['$scope', '$state', '$mdSidenav', '$reactive'];

    function SideNavController($scope, $state, $mdSidenav, $reactive) {
        var sidenav = this;

        activate();

        ////////////////

        function activate() {


            let reactiveContext = $reactive(sidenav).attach($scope);
            reactiveContext.helpers({
                isSuper: () => {
                    return Session.get('isSuper');
                },
                isAdmin: () => {
                    return Session.get('isAdmin');
                },
                isUser: () => {
                    return Session.get('isUser');
                }
            });
            reactiveContext.autorun(() => {
                sidenav.getReactively('isSuper');
                sidenav.getReactively('isAdmin');
                sidenav.getReactively('isUser');
            });

            sidenav.stateGo = (state) => {
                $state.go(state);
                sidenav.close('left');
            };

            sidenav.toggle = (div) => {
                return $mdSidenav(div).toggle();
            };

            sidenav.close = (div) => {
                return $mdSidenav(div).close();
            };

        }
    }

})();
