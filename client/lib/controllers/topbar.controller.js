(() => {
    'use strict';

    angular
        .module('app.topbar')
        .controller('TopBarController', TopBarController);

    TopBarController.$inject = ['$scope', '$mdSidenav', '$reactive'];

    function TopBarController($scope, $mdSidenav, $reactive) {
        var topbar = this;

        activate();

        ////////////////

        function activate() {

            let reactiveContext = $reactive(topbar).attach($scope);

            reactiveContext.helpers({
                email: () => {
                    return Session.get('email');
                },
                menuIsEnable: () => {
                    var isSuper = Session.get('isSuper');
                    var isAdmin = Session.get('isAdmin');
                    return isSuper || isAdmin;
                }
            });
            reactiveContext.autorun(() => {
                topbar.getReactively('email');
                topbar.getReactively('menuIsEnable');
            });

            topbar.sidenavToggle = (div) => {
                return $mdSidenav(div).toggle();
            };
        }
    }

})();
