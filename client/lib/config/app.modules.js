(() => {
    'use strict';

    angular.module('smsSender', [
        'angular-meteor',
        'angular-meteor.auth',
        'accounts.ui',
        'ngMaterial',
        'ngMessages',
        'ngSanitize',
        'ngFileUpload',
        'app.core',
        'app.smssender',
        'app.signage',
        'app.resource'
    ]);
/*Core*/
    angular.module('app.core', [
        'app.routes',
        'app.topbar',
        'app.sidenav',
        'app.access',
        'app.accounts'

    ]);

    angular.module('app.routes', ['ui.router']);
    angular.module('app.topbar', []);
    angular.module('app.sidenav', []);
    angular.module('app.access', []);
    angular.module('app.accounts', []);

/*SMS Sender*/
    angular.module('app.smssender', [
      'smssender.routes'
    ]);

    angular.module('smssender.routes', []);

/*Signage*/
    angular.module('app.signage', [
        'signage.routes',
        'signage.iframes',
        'signage.display',
        'signage.settings'
    ]);
    angular.module('signage.routes', []);
    angular.module('signage.iframes', []);
    angular.module('signage.display', []);
    angular.module('signage.settings', []);


/*Resource*/
    angular.module('app.resource', [
        'resource.routes',
        'resource.uploads'
    ]);
    angular.module('resource.routes', []);
    angular.module('resource.uploads', []);

})();
