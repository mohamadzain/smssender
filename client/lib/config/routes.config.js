(() => {
    'use strict';
    angular
        .module('app.routes')
        .config(($urlRouterProvider, $stateProvider, $locationProvider) => {
            $locationProvider.html5Mode(true);

            $urlRouterProvider.otherwise('/app/home');

            $stateProvider
                .state('app', {
                    url: '/app',
                    abstract: true,
                    templateUrl: 'client/lib/views/app.html'
                })
                .state('app.home', {
                    url: '/home',
                    templateUrl: 'client/lib/views/home.html'
                })
                .state('app.accounts', {
                    url: '/accounts',
                    abstract: true,
                    template: '<div data-ui-view=""></div>',
                    resolve: {
                        currentUser: ($q) => {
                            if(Meteor.userId() === null) {
                                return $q.reject('AUTH_REQUIRED');
                            } else {
                                return $q.resolve();
                            }
                        }
                    }
                })
                .state('app.accounts.list', {
                    url: '',
                    templateUrl: 'client/modules/accounts/views/accounts-main.html',
                    controller: 'AccountsController as accounts'
                });

        })
        .run(($rootScope, $state) => {
            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                if(error === 'AUTH_REQUIRED') {
                    $state.go('app.home');
                }
            });
        });

})();
