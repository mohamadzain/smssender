(() => {
    'use strict';

    angular
        .module('app.accounts')
        .controller('AccountsController', AccountsController);

    AccountsController.$inject = ['$scope', '$state', '$mdSidenav', '$mdToast', '$timeout', '$reactive'];

    function AccountsController($scope, $state, $mdSidenav, $mdToast, $timeout, $reactive) {
        var accounts = this;

        activate();

        ////////////////

        function activate() {


            let reactiveContext = $reactive(accounts).attach($scope);
            accounts.subscribe('expandedUserCollections');
            accounts.subscribe('expandedDeviceCollections');
            accounts.helpers({
                allUsers: () => {
                    return Meteor.users.find({
                        type: 'User'
                    });
                },
                allDevices: () => {
                    return Meteor.users.find({
                        type: 'Device'
                    });
                },
                isSuper: () => {
                    return Session.get('isSuper');
                }
            });

            reactiveContext.autorun(() => {
                accounts.getReactively('allUsers');
                accounts.getReactively('allDevices');
                accounts.getReactively('isSuper');

                console.log(accounts.allUsers);
            });


            accounts.submit = () => {
                accounts.tabListState = false;
                accounts.showProgresBar = true;
                if(accounts.form.type === 'User') {
                    accounts.form.username = null;
                    accounts.form.isUser = true;
                    Meteor.call('newUserAccount', accounts.form, (err) => {
                        accounts.showProgresBar = false;
                        if(err) {
                            showToast(err.reason, 'accountsToastError');
                        } else {
                            showToast('New user created', 'accountsToastSuccess');
                            $timeout(function() {
                                accounts.formRefresh();
                            }, 1000);
                        }
                    });
                } else
                if(accounts.form.type === 'Device') {
                    accounts.form.email = '';
                    accounts.form.password = '';
                    accounts.form.isUser = false;
                    accounts.form.isAdmin = false;
                    Meteor.call('newDeviceAccount', accounts.form, (err) => {
                        accounts.showProgresBar = false;
                        if(err) {
                            showToast(err.reason, 'accountsToastError');
                        } else {
                            showToast('New device created', 'accountsToastSuccess');
                            $timeout(function() {
                                accounts.formRefresh();
                            }, 1000);
                        }
                    });

                }

            };

            accounts.formRefresh = () => {
                accounts.tabListState = true;
                var t = accounts.form.type;
                accounts.form = {};
                $scope.accountsForm.$setPristine();
                $scope.accountsForm.$setUntouched();
                accounts.form.type = t;
            };

            var originatorEv;
            accounts.openUserListMenu = ($mdOpenMenu, ev) => {
                originatorEv = ev;
                $mdOpenMenu(ev);
            };

            accounts.deleteUser = (index) => {
                accounts.showListProgresBar = true;
                console.log(accounts.allUsers[index]._id);
                Meteor.call('removeUserAccount', accounts.allUsers[index]._id, (err) => {
                    accounts.showProgresBar = false;
                    console.log(err);
                    if(err) {
                        showToast(err.reason, 'accountsToastError');
                    } else {
                        showToast('user removed', 'accountsToastSuccess');
                        $timeout(function() {
                            accounts.formRefresh();
                        }, 1000);
                    }
                });
                originatorEv = null;
            };


            accounts.openDeviceListMenu = ($mdOpenMenu, ev) => {
                originatorEv = ev;
                $mdOpenMenu(ev);
            };

            accounts.deleteDevice = (index) => {
                accounts.showListProgresBar = true;
                console.log(accounts.allDevices[index]._id);
                Meteor.call('removeDeviceAccount', accounts.allDevices[index]._id, (err) => {
                    accounts.showProgresBar = false;
                    console.log(err);
                    if(err) {
                        showToast(err.reason, 'accountsToastError');
                    } else {
                        showToast('user removed', 'accountsToastSuccess');
                        $timeout(function() {
                            accounts.formRefresh();
                        }, 1000);
                    }
                });
                originatorEv = null;
            };


        }

        var last = {
            bottom: true,
            top: false,
            left: true,
            right: false
        };
        var toastPosition = angular.extend({}, last);

        function getToastPosition() {
            sanitizePosition();
            return Object.keys(toastPosition)
                .filter(function(pos) {
                    return toastPosition[pos];
                })
                .join(' ');
        }
        /*jshint -W116 */
        function sanitizePosition() {
            var current = toastPosition;
            if(current.bottom && last.top) {
                current.top = false;
            }
            if(current.top && last.bottom) {
                current.bottom = false;
            }
            if(current.right && last.left) {
                current.left = false;
            }
            if(current.left && last.right) {
                current.right = false;
            }
            last = angular.extend({}, current);
        }

        function showToast(message, element) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .hideDelay(3000)
                .position(getToastPosition())
                .parent(angular.element(element))
            );
        }

    }

})();
