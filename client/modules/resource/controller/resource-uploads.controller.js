(() => {
    /*jshint -W027*/
    'use strict';

    angular
        .module('resource.uploads')
        .controller('ResourceUploadsController', ResourceUploadsController);

    ResourceUploadsController.$inject = ['$rootScope', '$scope', '$element', '$reactive', '$mdToast', '$mdDialog'];

    function ResourceUploadsController($rootScope, $scope, $element, $reactive, $mdToast, $mdDialog) {
        var resourceUploads = this;

        activate();

        ////////////////

        function activate() {

            let reactiveContext = $reactive(resourceUploads).attach($scope);
            resourceUploads.subscribe('webCollections');
            resourceUploads.helpers({
                webFiles: () => {
                    return Web.find({}).fetch();
                }
            });

            reactiveContext.autorun(() => {
                resourceUploads.getReactively('webFiles');
            });


            resourceUploads.clearForm = () => {
                $rootScope.$broadcast('resourceClearForm');
            };


            var originatorEv;
            resourceUploads.openListMenu = ($mdOpenMenu, ev) => {
                originatorEv = ev;
                $mdOpenMenu(ev);
            };

            resourceUploads.deleteIframe = (index) => {
                resourceUploads.showListProgresBar = true;
                Web.remove({
                    _id: resourceUploads.webFiles[index]._id
                }, function(err) {
                    resourceUploads.showListProgresBar = false;
                    if(err) {
                        showToast(err.reason, 'resourceUploadsToastError');
                    } else {
                        showToast('Frame removed', 'resourceUploadsToastSuccess');
                    }
                });
                originatorEv = null;
            };


        }



        var last = {
            bottom: true,
            top: false,
            left: false,
            right: true
        };
        var toastPosition = angular.extend({}, last);

        function getToastPosition() {
            sanitizePosition();
            return Object.keys(toastPosition)
                .filter(function(pos) {
                    return toastPosition[pos];
                })
                .join(' ');
        }
        /*jshint -W116 */
        function sanitizePosition() {
            var current = toastPosition;
            if(current.bottom && last.top) {
                current.top = false;
            }
            if(current.top && last.bottom) {
                current.bottom = false;
            }
            if(current.right && last.left) {
                current.left = false;
            }
            if(current.left && last.right) {
                current.right = false;
            }
            last = angular.extend({}, current);
        }

        function showToast(message, element) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .hideDelay(3000)
                .position(getToastPosition())
                .parent(angular.element(element))
            );
        }
    }
})();
