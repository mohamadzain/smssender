(() => {
    /*jshint -W027*/
    'use strict';

    angular
        .module('resource.uploads')
        .controller('ResourceUploadsFormController', ResourceUploadsFormController);

    ResourceUploadsFormController.$inject = ['$rootScope', '$scope', '$element', '$reactive'];

    function ResourceUploadsFormController($rootScope, $scope, $element, $reactive) {
        var resourceUploadsForm = this;

        activate();

        ////////////////

        function activate() {

            let reactiveContext = $reactive(resourceUploadsForm).attach($scope);
            resourceUploadsForm.subscribe('webCollections');
            resourceUploadsForm.helpers({
                webFiles: () => {
                    return Web.find({}).fetch();
                }
            });

            reactiveContext.autorun(() => {
                resourceUploadsForm.getReactively('webFiles');
            });

            resourceUploadsForm.uploadFiles = function(webFiles) {
                if(webFiles && webFiles.length) {
                    resourceUploadsForm.newWebFileslist = [];
                    angular.forEach(webFiles, function(webFile) {
                        /*check if the file already exist*/
                        var found = _.find(resourceUploadsForm.webFiles, (uploadedWebFile) => {
                            return uploadedWebFile.name() === webFile.name;
                        });
                        /*delete the existing file before uploading new*/
                        console.log(found);
                        if(found) {
                            Web.remove({
                                _id: found._id
                            }, function(err) {
                                if(err) {
                                    console.log(err);
                                }
                            });
                        }
                        /*insert metada to file*/
                        var webFileWithMeta = new FS.File(webFile);




                        webFileWithMeta.metadata = {
                            label: resourceUploadsForm.form.label,
                        };
                        /*overwrite files */
                        Web.insert(webFileWithMeta, function(err, fileObj) {
                            /*get file info to display after upload*/
                            var f = {
                                name: fileObj.name(),
                                type: fileObj.type(),
                                size: fileObj.size()
                            };
                            resourceUploadsForm.newWebFileslist.push(f);
                        });

                    });

                }
            };

            $scope.$on('resourceClearForm', function() {
                resourceUploadsForm.form = {};
                document.getElementById('formContainer').reset();
                $scope.formContainer.$setPristine();
                $scope.formContainer.$setUntouched();
            });
        }
    }
})();
