(() => {
    'use strict';
    angular
        .module('resource.routes')
        .config(($urlRouterProvider, $stateProvider, $locationProvider) => {
            $locationProvider.html5Mode(true);

            $stateProvider
                .state('app.resource', {
                    url: '/resource',
                    templateUrl: 'client/modules/resource/views/resource-uploads.html',
                    controller: 'ResourceUploadsController as resourceUploads'
                });

        });
})();
