(() => {
    'use strict';
    angular
        .module('signage.routes')
        .config(($urlRouterProvider, $stateProvider, $locationProvider) => {
            $locationProvider.html5Mode(true);

            $stateProvider
                .state('signage', {
                    url: '/signage',
                    abstract: true,
                    template: '<div style="overflow:hidden; height:100%; width:100%" data-ui-view=""></div>'
                })
                .state('signage.display', {
                    url: '/display/{displayId}/{viewId}',
                    templateUrl: 'client/modules/signage/views/signage-display.html',
                    controller: 'SignageDisplayController as signageDisplay'
                })
                .state('app.signage', {
                    url: '/signage',
                    templateUrl: 'client/modules/signage/views/signage-settings.html',
                    controller: 'SignageSettingsController as signageSettings'
                });

        });
})();
