(() => {
    'use strict';
    /*jshint -W098*/
    /*jshint -W027*/
    angular
        .module('signage.iframes')
        .directive('signageIframe', signageIframe);

    signageIframe.$inject = ['$templateRequest', '$compile'];

    function signageIframe($templateRequest, $compile) {

        var directive = {
            link: SignageIframeLink,
        };
        return directive;

        function SignageIframeLink(scope, element) {


            scope.$on('drawFrame', function(ev, iFrame) {
                var t;

                if((/html/g).test(iFrame.resourceType)) {
                    t = '<iframe id="' + iFrame._id +
                        '" style="overflow:hidden; height:{{signageIframe[\'' + iFrame._id +
                        '\'].height}}px; width:{{signageIframe[\'' + iFrame._id +
                        '\'].width}}px; position:absolute; top:{{signageIframe[\'' + iFrame._id +
                        '\'].positionTop}}px;  left:{{signageIframe[\'' + iFrame._id +
                        '\'].positionLeft}}px;  z-index:{{signageIframe[\'' + iFrame._id +
                        '\'].zIndex}}; border:0px;" ng-src="{{signageIframe[\'' + iFrame._id +
                        '\'].src}}" scrolling="no" sandbox="allow-same-origin allow-scripts"></iframe>';
                } else
                if((/video/g).test(iFrame.resourceType)) {
                    t = '<video id="' + iFrame._id +
                        '" style="overflow:hidden;  position:absolute; top:{{signageIframe[\'' + iFrame._id +
                        '\'].positionTop}}px;  left:{{signageIframe[\'' + iFrame._id +
                        '\'].positionLeft}}px; z-index:{{signageIframe[\'' + iFrame._id +
                        '\'].zIndex}}; border:0px;" ng-src={{signageIframe[\'' + iFrame._id +
                        '\'].src}}  height={{signageIframe[\'' + iFrame._id +
                        '\'].height}}px width={{signageIframe[\'' + iFrame._id +
                        '\'].width}}px  loop autoplay></video>';


                } else
                if((/image/g).test(iFrame.resourceType)) {
                    t = '<img id="' + iFrame._id +
                        '" style="position:absolute; top:{{signageIframe[\'' + iFrame._id +
                        '\'].positionTop}}px;  left:{{signageIframe[\'' + iFrame._id +
                        '\'].positionLeft}}px; z-index:{{signageIframe[\'' + iFrame._id +
                        '\'].zIndex}}; height:{{signageIframe[\'' + iFrame._id +
                        '\'].height}}px; width:{{signageIframe[\'' + iFrame._id +
                        '\'].width}}px;" ng-src={{signageIframe[\'' + iFrame._id +
                        '\'].src}}>';
                }
                var template = angular.element(t);
                var content = $compile(template)(scope);
                element.append(content);
            });
        }
    }


})();
