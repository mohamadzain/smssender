(() => {
    /*jshint -W027*/
    'use strict';

    angular
        .module('signage.settings')
        .controller('SignageSettingsController', SignageSettingsController);

    SignageSettingsController.$inject = ['$rootScope', '$scope', '$element', '$reactive', '$mdToast', '$mdDialog', '$timeout'];

    function SignageSettingsController($rootScope, $scope, $element, $reactive, $mdToast, $mdDialog, $timeout) {
        var signageSettings = this;

        activate();

        ////////////////

        function activate() {

            let reactiveContext = $reactive(signageSettings).attach($scope);
            signageSettings.subscribe('iframeCollections');
            signageSettings.subscribe('webCollections');
            signageSettings.helpers({
                iframes: () => {
                    return Iframes.find({});
                },
                webFiles: () => {
                    return Web.find({});
                },
            });

            reactiveContext.autorun(() => {
                signageSettings.getReactively('webFiles');
                signageSettings.getReactively('iframes');
                signageSettings.groupedByIframes = _.groupBy(signageSettings.iframes, (iframes) => {
                    return iframes.group;
                });
                signageSettings.iFramesGroupNames = _.keys(signageSettings.groupedByIframes);
                if(signageSettings.selectedGroup) {
                    signageSettings.refreshIframeList();
                }

            });


            signageSettings.selectWebfile = () => {
                var selectedWebfile = signageSettings.webFiles[signageSettings.selectedWebFileIndex];

                signageSettings.form.resourceUrl = selectedWebfile.url().split('?token')[0];
                signageSettings.form.resourceType = selectedWebfile.type();
                signageSettings.form.resourceName = selectedWebfile.name();
                signageSettings.form.resourceId = selectedWebfile._id;
            };


            signageSettings.addNewGroup = () => {
                signageSettings.showAddGroup = true;
            };

            signageSettings.cancelNewGroup = () => {
                signageSettings.showAddGroup = false;
            };

            signageSettings.submit = () => {
                signageSettings.tabListState = false;
                signageSettings.showNewProgresBar = true;
                console.log(signageSettings.form);
                Meteor.call('newSignageIframe', signageSettings.form, (err) => {
                    signageSettings.showNewProgresBar = false;
                    if(err) {
                        showToast(err.reason, 'signageSettingsToastError');
                    } else {
                        showToast('New frame created', 'signageSettingsToastSuccess');
                        $timeout(function() {
                            signageSettings.tabListState = true;
                            signageSettings.form = {};
                            document.getElementById('signageSettingsForm').reset();
                            signageSettings.formObj.$setPristine();
                            signageSettings.formObj.$setUntouched();
                        }, 1000);
                    }
                });
            };

            signageSettings.refreshIframeList = () => {
                signageSettings.iFramesGroupList = signageSettings.groupedByIframes[signageSettings.selectedGroup];
            };

            var originatorEv;
            signageSettings.openListMenu = ($mdOpenMenu, ev) => {
                originatorEv = ev;
                $mdOpenMenu(ev);
            };

            signageSettings.deleteIframe = (index) => {
                signageSettings.showListProgresBar = true;
                console.log(signageSettings.iFramesGroupList[index]);
                Meteor.call('removeSignageIframe', signageSettings.iFramesGroupList[index], (err) => {
                    signageSettings.showListProgresBar = false;
                    if(err) {
                        showToast(err.reason, 'signageSettingsToastError');
                    } else {
                        showToast('Frame removed', 'signageSettingsToastSuccess');
                    }
                });
                originatorEv = null;
            };

            var editIframeDialog;
            signageSettings.editIframe = (index, event) => {
                editIframeDialog = $mdDialog.show({
                    controller: EditIframeDialogController,
                    templateUrl: 'client/modules/signage/views/partials/signage-settings-list-menu-dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: event,
                    clickOutsideToClose: true,
                    locals: {
                        iframe: signageSettings.iFramesGroupList[index],
                        webFiles: signageSettings.webFiles

                    }
                }).then(function(iframe) {
                    signageSettings.showListProgresBar = true;
                    Meteor.call('updateSignageIframe', iframe, (err) => {
                        signageSettings.showListProgresBar = false;
                        if(err) {
                            showToast(err.reason, 'signageSettingsToastError');
                        } else {
                            showToast('Frame Edits Saved', 'signageSettingsToastSuccess');
                        }
                    });
                });
                originatorEv = null;
            };

        }

        function EditIframeDialogController($scope, $mdDialog, iframe, webFiles) {

            $scope.signageSettings = {};
            for(let i in webFiles) {
                if(webFiles[i]._id === iframe.resourceId) {
                    $scope.signageSettings.selectedWebFileIndex = i;
                    break;
                }
            }
            $scope.signageSettings.webFiles = webFiles;
            $scope.signageSettings.form = iframe;
            $scope.signageSettings.form.action = 'Update';

            $scope.signageSettings.selectWebfile = () => {
                var selectedWebfile = $scope.signageSettings.webFiles[$scope.signageSettings.selectedWebFileIndex];

                $scope.signageSettings.form.resourceUrl = selectedWebfile.url().split('?token')[0];
                $scope.signageSettings.form.resourceType = selectedWebfile.type();
                $scope.signageSettings.form.resourceName = selectedWebfile.name();
                $scope.signageSettings.form.resourceId = selectedWebfile._id;
            };

            $scope.signageSettings.submit = () => {
                $mdDialog.hide([$scope.signageSettings.form]);
            };

        }

        var last = {
            bottom: true,
            top: false,
            left: false,
            right: true
        };
        var toastPosition = angular.extend({}, last);

        function getToastPosition() {
            sanitizePosition();
            return Object.keys(toastPosition)
                .filter(function(pos) {
                    return toastPosition[pos];
                })
                .join(' ');
        }
        /*jshint -W116 */
        function sanitizePosition() {
            var current = toastPosition;
            if(current.bottom && last.top) {
                current.top = false;
            }
            if(current.top && last.bottom) {
                current.bottom = false;
            }
            if(current.right && last.left) {
                current.left = false;
            }
            if(current.left && last.right) {
                current.right = false;
            }
            last = angular.extend({}, current);
        }

        function showToast(message, element) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .hideDelay(3000)
                .position(getToastPosition())
                .parent(angular.element(element))
            );
        }

    }


})();
