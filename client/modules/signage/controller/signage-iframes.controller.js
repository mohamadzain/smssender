(() => {
    'use strict';
    /*jshint -W098*/
    /*jshint -W027*/
    angular
        .module('signage.iframes')
        .controller('SignageIframeController', SignageIframeController);

    SignageIframeController.$inject = ['$rootScope', '$scope', '$sce', '$element', '$state', '$stateParams', '$reactive', '$timeout', '$window'];

    function SignageIframeController($rootScope, $scope, $sce, $element, $state, $stateParams, $reactive, $timeout, $window) {
        var signageIframe = this;

        activate();

        ////////////////

        function activate() {

            var postMessageData = {};

            $window.onmessage = (event) => {
                if(event.data && event.data.action === 'getUpdates') {
                    postMessageData.sms = signageIframe.newSms;
                    postMessageData.meetings = signageIframe.meetings;
                    postMessage(postMessageData);
                }
            };


            let reactiveContext = $reactive(signageIframe).attach($scope);

            signageIframe.subscribe('meetingsCollections');
            signageIframe.subscribe('newSMSCollections');
            signageIframe.subscribe('iframeCollections');
            signageIframe.helpers({
                iframes: () => {
                    return Iframes.find({});
                },
                newSms: () => {
                    return Sms.find({});
                },
                meetings: () => {
                    return Meetings.find({});
                },
            });

            reactiveContext.autorun(() => {
                signageIframe.getReactively('iframes');
                signageIframe.groupedByIframes = _.groupBy(signageIframe.iframes, (iframes) => {
                    return iframes.group;
                });
                if(signageIframe.groupedByIframes[$stateParams.viewId]) {
                    signageIframe.fillFrames();
                }
                /*remove element when it has been deleted from signage settings*/
                Iframes.find({}).observeChanges({
                    removed: (id) => {
                        var element = document.getElementById(id);
                        if(element) {
                            element.remove();
                        }
                    },
                    changed: (id) => {
                        if(signageIframe.groupedByIframes[$stateParams.viewId]) {
                            signageIframe.redrawFrame(id);
                        }
                    },
                    added: (id) => {
                        if(signageIframe.groupedByIframes[$stateParams.viewId]) {
                            signageIframe.redrawFrame(id);
                        }
                    }
                });
                signageIframe.getReactively('newSms');
                Sms.find({}).observeChanges({
                    removed: (id) => {
                        console.log('sms removed: ' + id);
                    },
                    changed: (id) => {
                        console.log('sms changed: ' + id);
                    },
                    added: (id) => {
                        if(signageIframe.groupedByIframes[$stateParams.viewId]) {
                            postMessageData.sms = signageIframe.newSms;
                            postMessage(postMessageData);
                        }

                    }
                });
                signageIframe.getReactively('meetings');
                Meetings.find({}).observeChanges({
                    added: (id) => {
                        if(signageIframe.groupedByIframes[$stateParams.viewId]) {
                            postMessageData.meetings = signageIframe.meetings;
                            postMessage(postMessageData);
                        }
                    }
                });
            });

            signageIframe.redrawFrame = (id) => {
                for(let iFrame of signageIframe.groupedByIframes[$stateParams.viewId]) {
                    if(iFrame._id === id) {
                        drawFrame(iFrame);
                        /*image types need to refresh -- bug?*/
                        if((/image/g).test(iFrame.resourceType)) {
                            refreshFrame(iFrame);
                        }
                    }
                }
            };

            signageIframe.fillFrames = () => {
                /*all frames already drawn*/
                if(angular.element('signage-iframe').children().length > 0) {
                    return;
                }
                for(let iFrame of signageIframe.groupedByIframes[$stateParams.viewId]) {
                    drawFrame(iFrame);
                }
            };
        }

        function isChangeOfElement(elementNode, resourceType) {
            if((/video/g).test(resourceType) && elementNode !== 'VIDEO') {
                return true;
            }
            if((/image/g).test(resourceType) && elementNode !== 'IMG') {
                return true;
            }
            if((/html/g).test(resourceType) && elementNode !== 'IFRAME') {
                return true;
            }
            return false;
        }

        function refreshFrame(iFrame) {
            signageIframe[iFrame._id].zIndex = -1;
            $scope.$apply();
            $timeout(() => {
                signageIframe[iFrame._id].zIndex = iFrame.zIndex;
            });
        }

        function drawFrame(iFrame) {
            /*get the elememt*/
            var element = document.getElementById(iFrame._id);
            /*draw or redraw if element is not found of element if of type html*/
            if(!element) {
                $rootScope.$broadcast('drawFrame', iFrame);
                signageIframe[iFrame._id] = {};
            } else {
                if(isChangeOfElement(element.nodeName, iFrame.resourceType)) {
                    element.remove();
                    $rootScope.$broadcast('drawFrame', iFrame);
                    signageIframe[iFrame._id] = {};
                }

            }
            /*use _.extend and ng-src value must never be empty */
            signageIframe[iFrame._id] = _.extend(signageIframe[iFrame._id], iFrame);
            signageIframe[iFrame._id].src = $sce.trustAsResourceUrl(iFrame.resourceUrl);
            $scope.$apply();
        }

        function postMessage(message) {
            for(let iFrame of signageIframe.groupedByIframes[$stateParams.viewId]) {
                if((/html/g).test(iFrame.resourceType)) {
                    if(iFrame._id) {
                        var iFrameWindow = document.getElementById(iFrame._id).contentWindow;
                        if(iFrameWindow) {
                            iFrameWindow.postMessage({
                                message: message
                            }, '*');
                        }
                    }
                }
            }
        }
    }
})();
