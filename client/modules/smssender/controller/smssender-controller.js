(() => {
    /*jshint -W027*/
    'use strict';

    angular
        .module('signage.settings')
        .controller('SmsSenderController', SmsSenderController);

    SmsSenderController.$inject = ['$rootScope', '$scope', '$element', '$reactive', '$mdToast', '$mdDialog', '$timeout'];

    function SmsSenderController($rootScope, $scope, $element, $reactive, $mdToast, $mdDialog, $timeout) {
        var smsSender = this;

        activate();

        ////////////////

        function activate() {

            let reactiveContext = $reactive(smsSender).attach($scope);
            smsSender.subscribe('smsCollections');
            smsSender.subscribe('messageCollections');
            smsSender.subscribe('contactsCollections');
            smsSender.helpers({
                sms: () => {
                    return Sms.find({});
                },
                messages: () => {
                    return Messages.find({});
                },
                contacts: () => {
                    return Contacts.find({});
                }
            });

            reactiveContext.autorun(() => {
                smsSender.getReactively('sms');
                smsSender.groupedBySender = _.groupBy(smsSender.sms, (sms) => {
                    return sms.sender;
                });
                smsSender.smsSenders = _.keys(smsSender.groupedBySender);
                smsSender.groupedByStatus = _.groupBy(smsSender.sms, (sms) => {
                    return sms.status;
                });
                smsSender.smsStatus = _.keys(smsSender.groupedByStatus);
                smsSender.smsStatus.push('All');

                smsSender.getReactively('messages');

                smsSender.getReactively('contacts');
                smsSender.groupedByOwner = _.groupBy(smsSender.contacts, (contact) => {
                    return contact.owner;
                });

                smsSender.contactOwners = _.keys(smsSender.groupedByOwner);

            });

            smsSender.refreshSenderList = () => {
                var smsGroupSenderList = smsSender.groupedBySender[smsSender.selectedSender];
                smsSender.smsSenderList = _.filter(smsGroupSenderList, (sms) => {
                    if(smsSender.selectedStatus !== 'All') {
                        return sms.status === smsSender.selectedStatus;
                    }
                    return true;
                });
            };

            smsSender.refreshContactList = () => {
                smsSender.contactList = smsSender.groupedByOwner[smsSender.selectedOwner];
            };

        }


        var last = {
            bottom: true,
            top: false,
            left: false,
            right: true
        };
        var toastPosition = angular.extend({}, last);

        function getToastPosition() {
            sanitizePosition();
            return Object.keys(toastPosition)
                .filter(function(pos) {
                    return toastPosition[pos];
                })
                .join(' ');
        }
        /*jshint -W116 */
        function sanitizePosition() {
            var current = toastPosition;
            if(current.bottom && last.top) {
                current.top = false;
            }
            if(current.top && last.bottom) {
                current.bottom = false;
            }
            if(current.right && last.left) {
                current.left = false;
            }
            if(current.left && last.right) {
                current.right = false;
            }
            last = angular.extend({}, current);
        }

        function showToast(message, element) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .hideDelay(3000)
                .position(getToastPosition())
                .parent(angular.element(element))
            );
        }

    }


})();
