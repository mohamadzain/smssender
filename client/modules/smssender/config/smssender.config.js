(() => {
    'use strict';
    angular
        .module('smssender.routes')
        .config(($urlRouterProvider, $stateProvider, $locationProvider) => {
            $locationProvider.html5Mode(true);

            $stateProvider
                .state('app.smssender', {
                    url: '/smssender',
                    abstract: true,
                    template: '<div style="overflow:hidden; height:100%; width:100%" data-ui-view=""></div>',
                    resolve: {
                        currentUser: ($q) => {
                            if(Meteor.userId() === null) {
                                return $q.reject('AUTH_REQUIRED');
                            } else {
                                return $q.resolve();
                            }
                        }
                    }
                })
                .state('app.smssender.records', {
                    url: '/records',
                    templateUrl: 'client/modules/smssender/views/smssender-main.html',
                    controller: 'SmsSenderController as smsSender'
                })

        });
})();
