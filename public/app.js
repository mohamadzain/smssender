(function() {
    'use strict';

    angular.module('smsIframeApp', [
        'app.smsTextDisplay'
    ]);


    angular
        .module('app.smsTextDisplay', ['angular-marquee']);


    angular
        .module('app.smsTextDisplay')
        .controller('SMSInfoController', SMSInfoController);

    SMSInfoController.$inject = ['$scope', '$http'];

    function SMSInfoController($scope, $http) {
        var smsInfo = this;

        activate();

        ////////////////

        function activate() {
          $scope.scroll = true;
          $scope.duration = 10000;

            // var settings = {
            //   'async': true,
            //   'crossDomain': true,
            //   'url': 'http://192.168.1.221:8080/api/sms/status?sender=postman',
            //   'method': 'GET',
            //   'headers': {
            //     'content-type': 'application/json',
            //     'x-token': 'xWZWMpZQce9TckTbc',
            //     'cache-control': 'no-cache',
            //     'postman-token': '159aa523-d5c9-8563-7a23-2c25891b83c4'
            //   }
            // }
            //
            // $.ajax(settings).done(function (response) {
            //   console.log(response);
            // });

            // Simple GET request example:
            $http({
              method: 'GET',
              // url: 'http://192.168.1.221:8080/api/sms/status?signage=',
              url: 'http://192.168.99.100/api/sms/status?signage=true',
              headers: {
                'Content-Type': 'application/json',
      //          'x-token': 'xWZWMpZQce9TckTbc',
                'x-token': 'paTQT553txev6WXz6', //dev
              },
            }).then(function successCallback(response) {
                console.log(response);
                for(var data  of response.data){
                    console.log(data);
                         smsInfo.messages = data;

                }




                          //  $scope.$apply();
                // this callback will be called asynchronously
                // when the response is available
              }, function errorCallback(response) {

                console.log(response);
                // called asynchronously if an error occurs
                // or server returns response with an error status.
              });


            // var parent = $window.parent.window;
            // parent.postMessage({action:'getUpdates'},'*');
            // $window.onmessage = (event) => {
            //     smsInfo.messages = event.data.message;
            //     console.log(smsInfo.messages.meetings);
            //
            //     console.log(smsInfo.messages.sms);
            //     $scope.$apply();
            // };
        }
    }
})();
