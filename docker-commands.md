
#docker command without meteor bundle.
sudo docker run \
-e ROOT_URL=http://localhost \
-e MONGO_URL=mongodb://db:27017 \
-p 80:80 \
-v ${HOME}/logs:/built_app/programs/server/assets/app/logs \
--device=/dev/ttyS0:/dev/ttyS0 --rm \
--link db:db \
--name="smssender" \
zymplsi/smssender


#docker run command for db.
sudo docker run -d \
-p 27017:27017 \
--name db mongo


#docker run commands, for upstart
refer to
smssender.conf #upstart file
smssenderdb.conf #upstart file



#file to delete in-case of bad errors, need to reboot.
#e.g
#Error response from daemon: Could not find container for entity id
sudo docker stop db
sudo rm -rf /var/lib/docker/containers
sudo rm -rf /var/lib/docker/linkgraph.db


# check all docker processes including those not running
sudo docker ps -a

#location and filename for upstart files
cd /var/log/upstarts #logfile directory
cd /etc/init #upstart scripts


# create lixnux bundle for production
delete .meteor/local/isopacks folder
sudo meteor build --architecture=os.linux.x86_64 ./

# create lixnux bundle for dev on osx
delete .meteor/local/isopacks folder
sudo meteor build --architecture=os.osx.x86_64 ./


#location of the meteor bundle/
cd /opt/bundle


#******************************* Reference Notes ************************







sudo docker run -d \
-p 27017:27017 \
--name db mongo


sudo docker run \
-e ROOT_URL=http://localhost \
-e MONGO_URL=mongodb://db:27017 \
-p 80:80 \
-v ${HOME}/logs:/built_app/programs/server/assets/app/logs \
--device=/dev/ttyS0:/dev/ttyS0 --rm \
--link db:db \
--name="smssender" \
zymplsi/smssender




sudo docker run \
-e ROOT_URL=http://localhost \
-e MONGO_URL=mongodb://db:27017 \
-p 8080:80 \
-v ${HOME}/smssender/logs:/built_app/programs/server/assets/app/logs \
-v ${HOME}/smssender/app:/bundle \
--device=/dev/ttyS0:/dev/ttyS0 --rm \
--link db:db \
--name="smssender" \
meteorhacks/meteord:base


sudo docker run -d\
-e ROOT_URL=http://localhost \
-e MONGO_URL=mongodb://db:27017 \
-p 8080:80 \
-v ${HOME}/smssender/logs:/built_app/programs/server/assets/app/logs \
-v ${HOME}/smssender/app:/bundle \
--device=/dev/ttyS0:/dev/ttyS0 --rm \
--link db:db \
--name="smssender" \
meteorhacks/meteord:base

sudo docker run \
-e ROOT_URL=http://localhost \
-e MONGO_URL=mongodb://db:27017 \
-p 8080:80 \
-v ${HOME}/smssender/logs:/built_app/programs/server/assets/app/logs \
-v ${HOME}/smssender/app:/bundle \
--device=/dev/ttyS0:/dev/ttyS0 --rm \
--link db:db \
meteorhacks/meteord:base
